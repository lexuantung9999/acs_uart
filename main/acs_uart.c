#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include "battery_rs485.h"


static void acs_rs485_task(void *arg)
{
    get_basic_info();
}


void app_main(void)
{
    acs_uart_config();
    xTaskCreate(acs_rs485_task, "acs_rs485_task", 2048, NULL, 10, NULL);
    {
        /* code */
        get_battery_voltage();
        get_battery_current();
        get_remaining_capacity();
        get_percentage();
        vTaskDelay(2000/portTICK_PERIOD_MS);
    }
}
